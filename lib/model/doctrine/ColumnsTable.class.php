<?php

/**
 * ColumnsTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ColumnsTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ColumnsTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Columns');
    }
}
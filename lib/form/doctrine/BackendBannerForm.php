<?php
class BackendBannerForm extends BannersForm
{
  public function configure()
  {
    parent::configure();
 
	$file = null;
	if ($this->getObject()->image) {
		$file = '/web/uploads/images/banners/'.$this->getObject()->image;
	};
    $this->widgetSchema['image'] = new sfWidgetFormInputFileEditable(array(
      'label'     => 'Banner',
      'file_src'  => $file,
      'is_image'  => true,
      'edit_mode' => !$this->isNew(),
      'template'  => '<div>%file%<br />%input%<br />%delete% Eliminar imagen actual</div>',
    ));
	
    $this->validatorSchema['image_delete'] = new sfValidatorPass();
	
	// set the validator for this file. This is where we will specify the class that
	// will handle the file upload. Make sure you set it to whatever you name your class
	$this->setValidator('image', new sfValidatorFile(
		array(
		'max_size' => 500000,
		'mime_types' => 'web_images', //you can set your own of course
		'path' => sfConfig::get('sf_upload_dir').'/images/banners/',
		'required' => false,
		'validated_file_class' => 'sfValidatedBannerPhoto'
		)
	));
  }
}
?>
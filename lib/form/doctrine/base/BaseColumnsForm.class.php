<?php

/**
 * Columns form base class.
 *
 * @method Columns getObject() Returns the current form's model object
 *
 * @package    fmnuevomundo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseColumnsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'date'        => new sfWidgetFormInputText(),
      'subject'     => new sfWidgetFormInputText(),
      'title'       => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormInputText(),
      'staff_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Staff'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'date'        => new sfValidatorPass(),
      'subject'     => new sfValidatorString(array('max_length' => 15)),
      'title'       => new sfValidatorString(array('max_length' => 255)),
      'description' => new sfValidatorPass(),
      'staff_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Staff'))),
    ));

    $this->widgetSchema->setNameFormat('columns[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Columns';
  }

}

<?php

/**
 * Banners form base class.
 *
 * @method Banners getObject() Returns the current form's model object
 *
 * @package    fmnuevomundo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBannersForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'title'    => new sfWidgetFormInputText(),
      'link'     => new sfWidgetFormInputText(),
      'image'    => new sfWidgetFormInputText(),
      'position' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'    => new sfValidatorString(array('max_length' => 255)),
      'link'     => new sfValidatorString(array('max_length' => 255)),
      'image'    => new sfValidatorString(array('max_length' => 255)),
      'position' => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('banners[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Banners';
  }

}

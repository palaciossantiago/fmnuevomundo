<?php

/**
 * Program form base class.
 *
 * @method Program getObject() Returns the current form's model object
 *
 * @package    fmnuevomundo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProgramForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'program'  => new sfWidgetFormInputText(),
      'time'     => new sfWidgetFormInputText(),
      'staff'    => new sfWidgetFormInputText(),
      'image'    => new sfWidgetFormInputText(),
      'position' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'program'  => new sfValidatorString(array('max_length' => 100)),
      'time'     => new sfValidatorString(array('max_length' => 100)),
      'staff'    => new sfValidatorString(array('max_length' => 100)),
      'image'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position' => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('program[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Program';
  }

}

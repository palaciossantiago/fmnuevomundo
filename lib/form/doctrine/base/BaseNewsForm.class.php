<?php

/**
 * News form base class.
 *
 * @method News getObject() Returns the current form's model object
 *
 * @package    fmnuevomundo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseNewsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'date'        => new sfWidgetFormInputText(),
      'title'       => new sfWidgetFormInputText(),
      'short_desc'  => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormInputText(),
      'author'      => new sfWidgetFormInputText(),
      'source'      => new sfWidgetFormInputText(),
      'photo_one'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'       => new sfValidatorString(array('max_length' => 100)),
      'short_desc'  => new sfValidatorString(array('max_length' => 100)),
      'description' => new sfValidatorPass(),
      'author'      => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'source'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'photo_one'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('news[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'News';
  }

}

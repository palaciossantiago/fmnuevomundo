<?php
class BackendNewsForm extends NewsForm
{
  public function configure()
  {
    parent::configure();
 
	$file = null;
	if ($this->getObject()->photo_one) {
		$file = '/web/uploads/images/news/small_'.$this->getObject()->photo_one;
	};
    $this->widgetSchema['photo_one'] = new sfWidgetFormInputFileEditable(array(
      'label'     => 'Foto',
      'file_src'  => $file,
      'is_image'  => true,
      'edit_mode' => !$this->isNew(),
      'template'  => '<div>%file%<br />%input%<br />%delete% Eliminar imagen actual</div>',
    ));
	
	$this->widgetSchema['date'] = new sfWidgetFormI18nDate(
		array('culture' => 'es', 'format'=>'%day%/%month%/%year%')
	);
	
	$this->widgetSchema['description'] = new sfWidgetFormTextareaTinyMCE(array(
		'width' => 600,
		'height' => 300,
		'config' => 'theme: "simple"'
	));
	
	$this->validatorSchema['date'] = new sfValidatorDate(array(
      'date_format' => '~(?P<day>\d{2})/(?P<month>\d{2})/(?P<year>\d{4})~',
    ));
	
    $this->validatorSchema['photo_one_delete'] = new sfValidatorPass();
	
	// set the validator for this file. This is where we will specify the class that
	// will handle the file upload. Make sure you set it to whatever you name your class
	$this->setValidator('photo_one', new sfValidatorFile(
		array(
		'max_size' => 500000,
		'mime_types' => 'web_images', //you can set your own of course
		'path' => sfConfig::get('sf_upload_dir').'/images/news/',
		'required' => false,
		'validated_file_class' => 'sfValidatedNewsPhoto'
		)
	));
  }
}
?>
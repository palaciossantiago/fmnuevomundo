<?php
class BackendColumnsForm extends ColumnsForm
{
  public function configure()
  {
    parent::configure();
	
	$this->widgetSchema['date'] = new sfWidgetFormI18nDate(
		array('culture' => 'es', 'format'=>'%day%/%month%/%year%')
	);
	$this->validatorSchema['date'] = new sfValidatorDate(array(
      'date_format' => '~(?P<day>\d{2})/(?P<month>\d{2})/(?P<year>\d{4})~',
    ));
	
	$this->widgetSchema['description'] = new sfWidgetFormTextareaTinyMCE(array(
		'width' => 600,
		'height' => 300,
		'config' => 'theme: "simple"'
	));
  }
}
?>
<?php
class BackendStaffForm extends StaffForm
{
  public function configure()
  {
    parent::configure();
 
	$file = null;
	if ($this->getObject()->photo) {
		$file = '/web/uploads/images/staff/'.$this->getObject()->photo;
	};
    $this->widgetSchema['photo'] = new sfWidgetFormInputFileEditable(array(
      'label'     => 'Foto',
      'file_src'  => $file,
      'is_image'  => true,
      'edit_mode' => !$this->isNew(),
      'template'  => '<div>%file%<br />%input%<br />%delete% Eliminar imagen actual</div>',
    ));

    $this->validatorSchema['photo_delete'] = new sfValidatorPass();
	
	// set the validator for this file. This is where we will specify the class that
	// will handle the file upload. Make sure you set it to whatever you name your class
	$this->setValidator('photo', new sfValidatorFile(
		array(
		'max_size' => 500000,
		'mime_types' => 'web_images', //you can set your own of course
		'path' => sfConfig::get('sf_upload_dir').'/images/staff/',
		'required' => false,
		'validated_file_class' => 'sfValidatedStaffPhoto'
		)
	));
  }
}
?>
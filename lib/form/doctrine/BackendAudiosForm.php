<?php
class BackendAudiosForm extends AudiosForm
{
  public function configure()
  {
    parent::configure();
 
	$file = null;
	if ($this->getObject()->file) {
		$file = '/web/uploads/audios/'.$this->getObject()->file;
	};
    $this->widgetSchema['file'] = new sfWidgetFormInputFileEditable(array(
      'label'     => 'Archivo de audio',
      'file_src'  => $file,
      'is_image'  => false,
      'edit_mode' => !$this->isNew(),
      'template'  => '<div>%file%<br />%input%<br />%delete% Eliminar audio actual</div>',
    ));
	
	$this->widgetSchema['date'] = new sfWidgetFormI18nDate(
		array('culture' => 'es', 'format'=>'%day%/%month%/%year%')
	);
	
	$this->validatorSchema['date'] = new sfValidatorDate(array(
      'date_format' => '~(?P<day>\d{2})/(?P<month>\d{2})/(?P<year>\d{4})~',
    ));
	
    $this->validatorSchema['file_delete'] = new sfValidatorPass();
	
	// set the validator for this file. This is where we will specify the class that
	// will handle the file upload. Make sure you set it to whatever you name your class
	$this->setValidator('file', new sfValidatorFile(
		array(
		'max_size' => 104857600,
		'path' => sfConfig::get('sf_upload_dir').'/audios/',
		'required' => true
		)
	));
  }
}
?>
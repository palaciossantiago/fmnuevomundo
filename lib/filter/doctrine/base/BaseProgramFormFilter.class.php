<?php

/**
 * Program filter form base class.
 *
 * @package    fmnuevomundo
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseProgramFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'program'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'time'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'staff'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'    => new sfWidgetFormFilterInput(),
      'position' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'program'  => new sfValidatorPass(array('required' => false)),
      'time'     => new sfValidatorPass(array('required' => false)),
      'staff'    => new sfValidatorPass(array('required' => false)),
      'image'    => new sfValidatorPass(array('required' => false)),
      'position' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('program_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Program';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'program'  => 'Text',
      'time'     => 'Text',
      'staff'    => 'Text',
      'image'    => 'Text',
      'position' => 'Number',
    );
  }
}

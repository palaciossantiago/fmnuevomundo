<td class="sf_admin_text sf_admin_list_td_programPhoto">
  <?php echo get_partial('program/programPhoto', array('type' => 'list', 'program' => $program)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_program">
  <?php echo link_to($program->getProgram(), 'program_edit', $program) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_time">
  <?php echo $program->getTime() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_staff">
  <?php echo $program->getStaff() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_position">
  <?php echo $program->getPosition() ?>
</td>

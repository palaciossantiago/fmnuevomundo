<?php use_helper('I18N', 'Date') ?>
<?php include_partial('program/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Nueva programaci&oacute;n', array(), 'messages') ?></h1>

  <?php include_partial('program/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('program/form_header', array('program' => $program, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('program/form', array('program' => $program, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('program/form_footer', array('program' => $program, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>

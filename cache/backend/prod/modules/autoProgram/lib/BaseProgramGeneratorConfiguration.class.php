<?php

/**
 * program module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage program
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: configuration.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseProgramGeneratorConfiguration extends sfModelGeneratorConfiguration
{
  public function getActionsDefault()
  {
    return array();
  }

  public function getFormActions()
  {
    return array(  '_delete' => NULL,  '_list' => NULL,  '_save' => NULL,  '_save_and_add' => NULL,);
  }

  public function getNewActions()
  {
    return array();
  }

  public function getEditActions()
  {
    return array();
  }

  public function getListObjectActions()
  {
    return array(  '_edit' => NULL,  '_delete' => NULL,);
  }

  public function getListActions()
  {
    return array(  '_new' => NULL,);
  }

  public function getListBatchActions()
  {
    return array(  '_delete' => NULL,);
  }

  public function getListParams()
  {
    return '%%_programPhoto%% - %%=program%% - %%time%% - %%staff%% - %%position%%';
  }

  public function getListLayout()
  {
    return 'tabular';
  }

  public function getListTitle()
  {
    return 'Programaci&oacute;n';
  }

  public function getEditTitle()
  {
    return 'Modificar programaci&oacute;n';
  }

  public function getNewTitle()
  {
    return 'Nueva programaci&oacute;n';
  }

  public function getFilterDisplay()
  {
    return array(  0 => 'program',  1 => 'time',  2 => 'staff',);
  }

  public function getFormDisplay()
  {
    return array();
  }

  public function getEditDisplay()
  {
    return array();
  }

  public function getNewDisplay()
  {
    return array();
  }

  public function getListDisplay()
  {
    return array(  0 => '_programPhoto',  1 => '=program',  2 => 'time',  3 => 'staff',  4 => 'position',);
  }

  public function getFieldsDefault()
  {
    return array(
      'id' => array(  'is_link' => true,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'program' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Programa',),
      'time' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Horario',),
      'staff' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Staff',),
      'image' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Imagen',),
      'position' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Orden',),
    );
  }

  public function getFieldsList()
  {
    return array(
      'id' => array(),
      'program' => array(),
      'time' => array(),
      'staff' => array(),
      'image' => array(),
      'position' => array(),
      'programPhoto' => array(  'label' => 'Foto',),
    );
  }

  public function getFieldsFilter()
  {
    return array(
      'id' => array(),
      'program' => array(),
      'time' => array(),
      'staff' => array(),
      'image' => array(),
      'position' => array(),
    );
  }

  public function getFieldsForm()
  {
    return array(
      'id' => array(),
      'program' => array(),
      'time' => array(),
      'staff' => array(),
      'image' => array(),
      'position' => array(),
    );
  }

  public function getFieldsEdit()
  {
    return array(
      'id' => array(),
      'program' => array(),
      'time' => array(),
      'staff' => array(),
      'image' => array(),
      'position' => array(),
    );
  }

  public function getFieldsNew()
  {
    return array(
      'id' => array(),
      'program' => array(),
      'time' => array(),
      'staff' => array(),
      'image' => array(),
      'position' => array(),
    );
  }


  /**
   * Gets the form class name.
   *
   * @return string The form class name
   */
  public function getFormClass()
  {
    return 'BackendProgramForm';
  }

  public function hasFilterForm()
  {
    return true;
  }

  /**
   * Gets the filter form class name
   *
   * @return string The filter form class name associated with this generator
   */
  public function getFilterFormClass()
  {
    return 'programFormFilter';
  }

  public function getPagerClass()
  {
    return 'sfDoctrinePager';
  }

  public function getPagerMaxPerPage()
  {
    return 5;
  }

  public function getDefaultSort()
  {
    return array(null, null);
  }

  public function getTableMethod()
  {
    return '';
  }

  public function getTableCountMethod()
  {
    return '';
  }
}

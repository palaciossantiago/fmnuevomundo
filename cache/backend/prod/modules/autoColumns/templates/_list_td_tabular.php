<td class="sf_admin_text sf_admin_list_td_columnDate">
  <?php echo get_partial('columns/columnDate', array('type' => 'list', 'columns' => $columns)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_title">
  <?php echo link_to($columns->getTitle(), 'columns_edit', $columns) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_subject">
  <?php echo $columns->getSubject() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_staffName">
  <?php echo get_partial('columns/staffName', array('type' => 'list', 'columns' => $columns)) ?>
</td>

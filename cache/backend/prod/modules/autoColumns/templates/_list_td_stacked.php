<td colspan="4">
  <?php echo __('%%columnDate%% - %%title%% - %%subject%% - %%staffName%%', array('%%columnDate%%' => get_partial('columns/columnDate', array('type' => 'list', 'columns' => $columns)), '%%title%%' => link_to($columns->getTitle(), 'columns_edit', $columns), '%%subject%%' => $columns->getSubject(), '%%staffName%%' => get_partial('columns/staffName', array('type' => 'list', 'columns' => $columns))), 'messages') ?>
</td>

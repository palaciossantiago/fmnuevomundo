<?php use_helper('I18N', 'Date') ?>
<?php include_partial('columns/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Modificar columna', array(), 'messages') ?></h1>

  <?php include_partial('columns/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('columns/form_header', array('columns' => $columns, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('columns/form', array('columns' => $columns, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('columns/form_footer', array('columns' => $columns, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>

<td class="sf_admin_text sf_admin_list_td_newsPhoto">
  <?php echo get_partial('news/newsPhoto', array('type' => 'list', 'news' => $news)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_newsDate">
  <?php echo get_partial('news/newsDate', array('type' => 'list', 'news' => $news)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_title">
  <?php echo link_to($news->getTitle(), 'news_edit', $news) ?>
</td>

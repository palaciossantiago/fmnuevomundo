<?php

/**
 * news module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage news
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: configuration.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseNewsGeneratorConfiguration extends sfModelGeneratorConfiguration
{
  public function getActionsDefault()
  {
    return array();
  }

  public function getFormActions()
  {
    return array(  '_delete' => NULL,  '_list' => NULL,  '_save' => NULL,  '_save_and_add' => NULL,);
  }

  public function getNewActions()
  {
    return array();
  }

  public function getEditActions()
  {
    return array();
  }

  public function getListObjectActions()
  {
    return array(  '_edit' => NULL,  '_delete' => NULL,);
  }

  public function getListActions()
  {
    return array(  '_new' => NULL,);
  }

  public function getListBatchActions()
  {
    return array(  '_delete' => NULL,);
  }

  public function getListParams()
  {
    return '%%_newsPhoto%% - %%_newsDate%% - %%=title%%';
  }

  public function getListLayout()
  {
    return 'tabular';
  }

  public function getListTitle()
  {
    return 'Lista de noticias';
  }

  public function getEditTitle()
  {
    return 'Modificar noticia';
  }

  public function getNewTitle()
  {
    return 'Nueva noticia';
  }

  public function getFilterDisplay()
  {
    return array(  0 => 'date',  1 => 'title',);
  }

  public function getFormDisplay()
  {
    return array();
  }

  public function getEditDisplay()
  {
    return array(  0 => 'date',  1 => 'title',  2 => 'short_desc',  3 => 'description',  4 => 'photo_one',);
  }

  public function getNewDisplay()
  {
    return array(  0 => 'date',  1 => 'title',  2 => 'short_desc',  3 => 'description',  4 => 'photo_one',);
  }

  public function getListDisplay()
  {
    return array(  0 => '_newsPhoto',  1 => '_newsDate',  2 => '=title',);
  }

  public function getFieldsDefault()
  {
    return array(
      'id' => array(  'is_link' => true,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'date' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Fecha',),
      'title' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Titulo',),
      'short_desc' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Resumen',),
      'description' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Texto de noticia',),
      'author' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'source' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'photo_one' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Foto',),
    );
  }

  public function getFieldsList()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'short_desc' => array(),
      'description' => array(),
      'author' => array(),
      'source' => array(),
      'photo_one' => array(),
      'newsDate' => array(  'label' => 'Fecha',),
      'newsPhoto' => array(  'label' => 'Foto',),
    );
  }

  public function getFieldsFilter()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'short_desc' => array(),
      'description' => array(),
      'author' => array(),
      'source' => array(),
      'photo_one' => array(),
    );
  }

  public function getFieldsForm()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'short_desc' => array(),
      'description' => array(),
      'author' => array(),
      'source' => array(),
      'photo_one' => array(),
    );
  }

  public function getFieldsEdit()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'short_desc' => array(),
      'description' => array(),
      'author' => array(),
      'source' => array(),
      'photo_one' => array(),
    );
  }

  public function getFieldsNew()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'short_desc' => array(),
      'description' => array(),
      'author' => array(),
      'source' => array(),
      'photo_one' => array(),
    );
  }


  /**
   * Gets the form class name.
   *
   * @return string The form class name
   */
  public function getFormClass()
  {
    return 'BackendNewsForm';
  }

  public function hasFilterForm()
  {
    return true;
  }

  /**
   * Gets the filter form class name
   *
   * @return string The filter form class name associated with this generator
   */
  public function getFilterFormClass()
  {
    return 'NewsFormFilter';
  }

  public function getPagerClass()
  {
    return 'sfDoctrinePager';
  }

  public function getPagerMaxPerPage()
  {
    return 10;
  }

  public function getDefaultSort()
  {
    return array('title', 'desc');
  }

  public function getTableMethod()
  {
    return '';
  }

  public function getTableCountMethod()
  {
    return '';
  }
}

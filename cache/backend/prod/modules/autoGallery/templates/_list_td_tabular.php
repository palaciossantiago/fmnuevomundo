<td class="sf_admin_text sf_admin_list_td_newsPhoto">
  <?php echo get_partial('gallery/newsPhoto', array('type' => 'list', 'gallery' => $gallery)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_title">
  <?php echo link_to($gallery->getTitle(), 'gallery_edit', $gallery) ?>
</td>

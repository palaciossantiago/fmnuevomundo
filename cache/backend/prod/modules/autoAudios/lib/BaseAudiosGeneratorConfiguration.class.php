<?php

/**
 * audios module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage audios
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: configuration.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAudiosGeneratorConfiguration extends sfModelGeneratorConfiguration
{
  public function getActionsDefault()
  {
    return array();
  }

  public function getFormActions()
  {
    return array(  '_delete' => NULL,  '_list' => NULL,  '_save' => NULL,  '_save_and_add' => NULL,);
  }

  public function getNewActions()
  {
    return array();
  }

  public function getEditActions()
  {
    return array();
  }

  public function getListObjectActions()
  {
    return array(  '_edit' => NULL,  '_delete' => NULL,);
  }

  public function getListActions()
  {
    return array(  '_new' => NULL,);
  }

  public function getListBatchActions()
  {
    return array(  '_delete' => NULL,);
  }

  public function getListParams()
  {
    return '%%_newsDate%% - %%title%%';
  }

  public function getListLayout()
  {
    return 'tabular';
  }

  public function getListTitle()
  {
    return 'Lista de audios';
  }

  public function getEditTitle()
  {
    return 'Modificar audio';
  }

  public function getNewTitle()
  {
    return 'Nuevo audio';
  }

  public function getFilterDisplay()
  {
    return array(  0 => 'date',  1 => 'title',);
  }

  public function getFormDisplay()
  {
    return array();
  }

  public function getEditDisplay()
  {
    return array();
  }

  public function getNewDisplay()
  {
    return array();
  }

  public function getListDisplay()
  {
    return array(  0 => '_newsDate',  1 => 'title',);
  }

  public function getFieldsDefault()
  {
    return array(
      'id' => array(  'is_link' => true,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',),
      'date' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Fecha',),
      'title' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Titulo',),
      'description' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Descripci&oacute;n',),
      'file' => array(  'is_link' => false,  'is_real' => true,  'is_partial' => false,  'is_component' => false,  'type' => 'Text',  'label' => 'Archivo de audio',),
    );
  }

  public function getFieldsList()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'description' => array(),
      'file' => array(),
      'newsDate' => array(  'label' => 'Fecha',),
    );
  }

  public function getFieldsFilter()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'description' => array(),
      'file' => array(),
    );
  }

  public function getFieldsForm()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'description' => array(),
      'file' => array(),
    );
  }

  public function getFieldsEdit()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'description' => array(),
      'file' => array(),
    );
  }

  public function getFieldsNew()
  {
    return array(
      'id' => array(),
      'date' => array(),
      'title' => array(),
      'description' => array(),
      'file' => array(),
    );
  }


  /**
   * Gets the form class name.
   *
   * @return string The form class name
   */
  public function getFormClass()
  {
    return 'BackendAudiosForm';
  }

  public function hasFilterForm()
  {
    return true;
  }

  /**
   * Gets the filter form class name
   *
   * @return string The filter form class name associated with this generator
   */
  public function getFilterFormClass()
  {
    return 'audiosFormFilter';
  }

  public function getPagerClass()
  {
    return 'sfDoctrinePager';
  }

  public function getPagerMaxPerPage()
  {
    return 10;
  }

  public function getDefaultSort()
  {
    return array(null, null);
  }

  public function getTableMethod()
  {
    return '';
  }

  public function getTableCountMethod()
  {
    return '';
  }
}

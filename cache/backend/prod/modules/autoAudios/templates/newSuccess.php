<?php use_helper('I18N', 'Date') ?>
<?php include_partial('audios/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Nuevo audio', array(), 'messages') ?></h1>

  <?php include_partial('audios/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('audios/form_header', array('audios' => $audios, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('audios/form', array('audios' => $audios, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('audios/form_footer', array('audios' => $audios, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>

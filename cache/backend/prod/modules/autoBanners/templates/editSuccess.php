<?php use_helper('I18N', 'Date') ?>
<?php include_partial('banners/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Modificar banner', array(), 'messages') ?></h1>

  <?php include_partial('banners/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('banners/form_header', array('banners' => $banners, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('banners/form', array('banners' => $banners, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('banners/form_footer', array('banners' => $banners, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>

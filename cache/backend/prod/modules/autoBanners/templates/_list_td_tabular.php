<td class="sf_admin_text sf_admin_list_td_bannerPhoto">
  <?php echo get_partial('banners/bannerPhoto', array('type' => 'list', 'banners' => $banners)) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_title">
  <?php echo link_to($banners->getTitle(), 'banners_edit', $banners) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_position">
  <?php echo $banners->getPosition() ?>
</td>

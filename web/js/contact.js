window.onload = function(){
    var options = {
        zoom: 16
        , center: new google.maps.LatLng(-30.943458, -61.552954)
        , mapTypeId: google.maps.MapTypeId.SATELLITE
        , keyboardShortcuts: false
        , disableDoubleClickZoom: true
        , scrollwheel: false
        , draggableCursor: 'move'
        , draggingCursor: 'move'
        , mapTypeControl: false
        , navigationControl: false
        , streetViewControl: false
        , scaleControl: true
    };
    var map = new google.maps.Map(document.getElementById('map'), options);
	var myLatlng = new google.maps.LatLng(-30.943458, -61.552954);
	var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Radio Fm Nuevo Mundo 106.9 Mhz'
	});
};
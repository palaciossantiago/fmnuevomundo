<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>FM Nuevo Mundo | Panel de control</title>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php use_stylesheet('admin.css') ?>
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>
  </head>
  <body>
    <div id="container">
      <div id="header">&nbsp;</div>
      <div id="menu">
	  <?php if ($sf_user->isAuthenticated()): ?>
        <ul>
          <li>
			<?php echo link_to('Noticias', 'news') ?>
          </li>
          <li>
			<?php echo link_to('Notas', 'featurednews') ?>
          </li>		  
          <li>
			<?php echo link_to('Staff', 'staff') ?>
          </li>
          <li>
			<?php echo link_to('Programaci&oacute;n', 'program') ?>
          </li>
          <li>
			<?php echo link_to('Galer&iacute;a', 'gallery') ?>
          </li>
          <li>
			<?php echo link_to('Columnas', 'columns') ?>
          </li>		  
          <li>
			<?php echo link_to('Audios', 'audios') ?>
          </li>		  
          <li>
			<?php echo link_to('Banners', 'banners') ?>
          </li>
		  <li>
			<?php echo link_to('Usuarios', 'sf_guard_user') ?>
		  </li>
		  <li>
			<?php echo link_to('Cerrar sesi&oacute;n', 'sf_guard_signout') ?>
		  </li>
        </ul>
	 <?php endif ?>
      </div>
      <div id="content">
        <?php echo $sf_content ?>
      </div>
    </div>
  </body>
</html>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body>
	<!--[if lt IE 7]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
   <div id="wrapper">
         <!-- Begin Header -->
         <div id="header">
			<header class="header wrapper clearfix">
				<?php echo link_to(image_tag('live.png', 'id=live title=Escuch&aacute; la radio en vivo'), 'live/index') ?>
            </header>
			<ul id="menu">
				<li><?php echo link_to("Institucional", 'home/index') ?></li>
				<li><?php echo link_to("Noticias", 'news/index') ?></li>
				<li><?php echo link_to("Audios", 'audios/index') ?></li>
				<li><?php echo link_to("En vivo", 'live/index') ?></li>
				<li><?php echo link_to("Programaci&oacute;n", 'program/index') ?></li>
				<li><?php echo link_to("Staff", 'staff/index') ?></li>
				<li><?php echo link_to("Contacto", 'contact/index') ?></li>
			</ul>
		 </div>
		 <!-- End Header -->
		 
         <!-- Begin Faux Columns -->
		 <div id="faux">
		       <!-- Begin Left Column -->
		       <div id="leftcolumn">
					<div class="block">
						<div class="tag">
							<?php echo image_tag('tag_left.png', 'class=tag-left') ?>
							<h3 class="tag-content">Columnas</h3>
							<?php echo image_tag('tag_right.png', 'class=tag-right') ?>
						</div>
						<?php foreach(get_slot("columns") as $item): ?>
						<div class="columnista">
							<?php if ($item->photo) { ?>
							<?php echo link_to(image_tag('/uploads/images/staff/'.$item->photo, 'alt='.$item->name.' title='.$item->name.' width="72" height="72"'), 'columns/detail?id='.$item->Columns[0]->id) ?>
							<?php } ?>
							<p><?php echo link_to('<strong>'.$item->name.'</strong><br/>'.$item->occupation, 'columns/detail?id='.$item->Columns[0]->id, 'title='.$item->name) ?></p>
						</div>
						<?php endforeach ?>					
					</div>
					<div class="block">
						<div class="tag">
							<?php echo image_tag('tag_left.png', 'class=tag-left') ?>
							<h3 class="tag-content">Notas destacadas</h3>
							<?php echo image_tag('tag_right.png', 'class=tag-right') ?>
						</div>
						<ul class="destacada">
							<?php foreach(get_slot("notes") as $item): ?>
							<li>
							<?php echo link_to($item->title, 'notes/detail?id='.$item->id, 'title='.$item->title) ?>
							</li>
							<?php endforeach ?>							
						</ul>
						<?php echo link_to('M&aacute;s', 'notes/index', 'title="Ver m&aacute;s notas" class=destacada-more') ?>
					</div>
					<div class="block">
						<div class="tag">
							<?php echo image_tag('tag_left.png', 'class=tag-left') ?>
							<h3 class="tag-content">Redes sociales</h3>
							<?php echo image_tag('tag_right.png', 'class=tag-right') ?>
						</div>
						<a href="http://www.facebook.com/fmnuevomundo" class="social-icon" title="Facebook">
							<?php echo image_tag('facebook_icon.png', 'alt=facebook') ?>
						</a>
						<a href="http://www.twitter.com/fmnuevomundo" class="social-icon" title="Twitter">
							<?php echo image_tag('twitter_icon.png', 'alt=twitter') ?>
						</a>
						<a href="#" class="social-icon" title="Youtube">
							<?php echo image_tag('youtube_icon.png', 'alt=youtube') ?>
						</a>
					</div>	
		       </div>
		       <!-- End Left Column -->	
	<!-- Begin Content Column -->
	<div id="content">
		<?php echo $sf_content ?>
	</div>
	<!-- End Content Column -->
	<!-- Begin Right Column -->
	<div id="rightcolumn">
		<?php foreach(get_slot("banners") as $item): ?>
		<?php echo link_to(image_tag('/uploads/images/banners/'.$item->image, 'alt='.$item->title.' title='.$item->title), $item->link, 'class=banner target=blank') ?>
		<?php endforeach ?>	
	</div>
	<!-- End Right Column -->
	</div>	   
	<!-- End Faux Columns --> 
		
	<!--<img class="banner_bottom" src="/web/uploads/images/banner06.jpg" />-->
	<div class="clear">&nbsp;</div>	
	 <!-- Begin Footer -->
	 <div id="footer">
		<footer class="wrapper">
			
			<p>2001-2014 <strong>FM Nuevo Mundo</strong> es marca registrada. Derechos Reservados.</p>
		</footer>
	 </div>
	 <!-- End Footer -->
		 
   </div>
   <!-- End Wrapper -->
   
           <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>-->
			
  </body>
</html>
<?php

class fmNuevoMundoActions extends sfActions {

	public function preExecute() {
		$request = $this->getRequest(); 
		// Columnistas columna izquierda
		$columns = Doctrine_Query::create()
			->from('Staff s')
			->select('s.id, s.name, s.occupation, c.id')
			->innerJoin('s.Columns c')
			//->orderBy('c.date DESC')
			->limit(4)
			->execute();
		$this->getResponse()->setSlot("columns", $columns);
		// Notas destacadas
		$notes = Doctrine_Query::create()
			->from('FeaturedNews f')
			->select('f.id, f.title')
			->orderBy('f.date DESC')
			->limit(5)
			->execute();
		$this->getResponse()->setSlot("notes", $notes);
		// Banners de publicidad
		$banners = Doctrine_Query::create()
			->from('Banners b')
			->select('b.id, b.title, b.link, b.image')
			->orderBy('b.position ASC')
			->execute();
		$this->getResponse()->setSlot("banners", $banners);		
	}
}
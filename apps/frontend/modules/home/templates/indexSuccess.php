<h2>Institucional</h2>
<p><strong>FM NUEVO MUNDO 106.9</strong>, les da la bienvenida a su sitio web.</p>
<p>Esta estaci&oacute;n de radio propone una selecci&oacute;n musical basada en cl&aacute;sicos de los a&ntilde;os 70 en adelante, nacionales e internacionales, rock, anglo y pop. Los &eacute;xitos que hicieron historia llegando hasta la m&uacute;sica de hoy.</p>
<p>La propuesta apunta a ser cordial compa&ntilde;ia. Con una ma&ntilde;ana informativa, y una programaci&oacute;n que hace incapi&eacute; en la m&uacute;sica, pero con flashes informativos que no permiten aislarse de la realidad.	</p>
<p><strong>NUEVO MUNDO</strong> es la emisora con m&aacute;xima popularidad en transmisiones deportivas nacionales e internacionales apoyando a las distintas disciplinas locales.</p>
<p><strong>AS&Iacute; ES NUEVO MUNDO, LO INVITAMOS A DISFRUTARLA...</strong></p>
<div class="separator">&nbsp;</div>
<?php if (sizeof($gallery) > 0) { ?>
<h2>Galer&iacute;a</h2>
<div id="gallery">
	<?php foreach($gallery as $index => $item): ?>
	<?php if (($index + 1) % 3 == 0) { ?>
	<div class="child last">
	<?php } else { ?>
	<div class="child">
	<?php } ?>	
	<a href="uploads/images/gallery/<?php echo $item->image ?>" class="fancybox" rel="gallery" title="<?php echo $item->title ?>"><?php echo image_tag('/uploads/images/gallery/small_'.$item->image, 'alt="'.$item->title.'" title="'.$item->title.'" width=130 height=90') ?></a>
	</div>			
	<?php endforeach ?>	
</div>
<?php } ?>
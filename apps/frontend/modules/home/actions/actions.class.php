<?php

/**
 * home actions.
 *
 * @package    fmnuevomundo
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends fmNuevoMundoActions {

	public function executeIndex(sfWebRequest $request) {
		// Galeria de fotos de home
		$this->gallery = Doctrine_Query::create()
			->from('Gallery g')
			->select('g.id, g.title, g.image')
			->execute();
		sfView::SUCCESS;
	}
}

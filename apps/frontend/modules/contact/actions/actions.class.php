<?php

/**
 * contact actions.
 *
 * @package    fmnuevomundo
 * @subpackage contact
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class contactActions extends fmNuevoMundoActions {

	public function executeIndex(sfWebRequest $request) {
		$this->error = null;
		$this->ok = null;
	}
	
	public function executeSubmit(sfWebRequest $request) {
		$this->setTemplate('index');
		$this->error = null;
		$this->ok = null;
		$name = $request->getParameter("name");
		$email = $request->getParameter("email");
		$subject = $request->getParameter("subject");
		$phone = $request->getParameter("phone");
		$message = $request->getParameter("message");
		if ($name == null || $email == null || $message == null) {
			$this->error = "Por favor, debe completar todos los campos obligatorios.";
		} else {
			$to      = 'contacto@fmnuevomundo.com.ar';
			$subject = 'FM Nuevo Mundo | Consulta Sitio Web | ' . $subject;
			$mensaje = "Nombre: " . $name . "\n";
			$mensaje .= "Email: " . $email . "\n";
			$mensaje .= "Telefono: " . $phone . "\n";
			$mensaje .= "Mensaje: " . $message . "\n";
			$headers = 'From: ' . $email . "\r\n" .
				'Reply-To: ' . $email . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
			mail($to, $subject, $mensaje, $headers);
			$this->ok = "Su consulta ha sido enviada correctamente. Gracias por contactarse con nosotros!";
		}
	}	
}

<h2>Contacto</h2>
<?php if ($error) { ?>
<p class="error"><?php echo $error ?></p>
<?php } ?>
<?php if ($ok) { ?>
<p class="ok"><?php echo $ok ?></p>
<?php } ?>
<form class="contacto" name="contact" action="<?php echo url_for('contact/submit') ?>" method="POST">
	<label for="name">Nombre *</label>
	<input type="text" name="name" value="" />
	<label for="email">Email *</label>
	<input type="text" name="email" value="" />
	<label for="subject">Asunto</label>
	<input type="text" name="subject" value="" />
	<label for="phone">Tel&eacute;fono</label>
	<input type="text" name="phone" value="" />
	<label for="message">Mensaje *</label>
	<textarea name="message"></textarea>
	<input type="submit" value="Enviar" />
</form>
<div id="map"></div>
<div class="contact-left-column">
<p class="strong">Avenida H. Irigoyen 1212</p>
<p>Planta baja</p>
<p>Sunchales, Sante Fe</p>
</div>
<div class="contact-right-column">
<p class="strong">(03493) 426522 - 426523</p>
</div>
<?php

/**
 * audios actions.
 *
 * @package    fmnuevomundo
 * @subpackage audios
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class audiosActions extends fmNuevoMundoActions {

	public function executeIndex(sfWebRequest $request) {
		// Lista de audios
		$audiosQuery = Doctrine_Query::create()
			->from('Audios n')
			->select('n.id, n.date, n.title, n.description, n.file')
			->orderBy('n.date DESC');
		$this->pager = new sfDoctrinePager('news', 5);
		$this->pager->setQuery($audiosQuery);
		$this->pager->setPage($request->getParameter('page', 1));
		$this->pager->init();	
		$this->audios = $this->pager->getResults();
	}
}

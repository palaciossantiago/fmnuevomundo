<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=152118818298123";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<h2>Audios</h2>
<?php if (sizeof($audios) > 0) { ?>
	<?php foreach($audios as $item): ?>
	<div class="noticia">
		<h3><?php echo $item->title ?></h3>
		<p><?php echo $item->description ?></p>
		<audio src="uploads/audios/<?php echo $item->file; ?>" preload="auto" ></audio>
		<div class="clear">&nbsp;</div>
		
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://fmnuevomundo.com.ar/web/audios" data-lang="es">Twittear</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

		<div class="fb-like" data-href="http://fmnuevomundo.com.ar/web/audios" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
		
		<div class="clear"></div>
	</div>
	<?php endforeach ?>
	<?php if ($pager->haveToPaginate())  { ?>
	<div class="pagination" style="font-family: arial">
	<?php echo link_to('&#9668;', 'audios/index?page='.$pager->getPreviousPage(), 'class=pagination-arrow') ?>
	<?php $links = $pager->getLinks(); foreach ($links as $page): ?>
	<?php echo ($page == $pager->getPage()) ? '<span>'.$page.'</span>' : link_to($page, 'audios/index?page='.$page) ;?>
	<?php if ($page != $pager->getCurrentMaxLink()): ?> <?php endif ?>
	<?php endforeach ?>
	<?php echo link_to('&#9658;', 'audios/index?page='.$pager->getNextPage(), 'class=pagination-arrow') ?>
	</div>
	<?php } ?>
<?php } else { ?>
	<p>Actualmente no hay audios para escuchar.</p>
<?php } ?>
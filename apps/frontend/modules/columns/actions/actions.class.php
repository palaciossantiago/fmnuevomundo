<?php

/**
 * columns actions.
 *
 * @package    fmnuevomundo
 * @subpackage columns
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class columnsActions extends fmNuevoMundoActions {
  
	public function executeIndex(sfWebRequest $request) {

	}
  
	public function executeDetail(sfWebRequest $request) {
		$id = $request->getParameter('id', -1);
		$this->item = Doctrine::getTable('Columns')->find($id);
		if ($this->item == null) {
			return sfView::ERROR;
		} else {
			return sfView::SUCCESS;
		}
	}  
}

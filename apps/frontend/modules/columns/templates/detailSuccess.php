<div class="detalle-noticia">
	<?php use_helper('Date'); ?>
	<?php if ($item->date != null && $item->date != "") { ?>
		<p class="date"><?php echo format_date($item->date, 'dd/MM/yyyy'); ?></p>
	<?php } ?>
	<h2><?php echo $item->title ?></h2>
	<p><?php echo $item->getDescription(ESC_RAW) ?></p>
</div>
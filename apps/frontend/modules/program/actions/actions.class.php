<?php

/**
 * program actions.
 *
 * @package    fmnuevomundo
 * @subpackage program
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class programActions  extends fmNuevoMundoActions {

	public function executeIndex(sfWebRequest $request) {
		// Lista de programas
		$programQuery = Doctrine_Query::create()
			->from('Program p')
			->select('p.id, p.program, p.time, p.staff, p.image')
			->orderBy('p.position ASC');
		$this->pager = new sfDoctrinePager('Program', 9);
		$this->pager->setQuery($programQuery);
		$this->pager->setPage($request->getParameter('page', 1));
		$this->pager->init();	
		$this->programs = $this->pager->getResults();
	}
}
<h2>Programaci&oacute;n</h2>
<?php if (sizeof($programs) > 0) { ?>
	<h3>Lunes a viernes</h3>
	<div id="program">
	<?php foreach($programs as $index => $item): ?>
	<?php if (($index + 1) % 3 == 0) { ?>
	<div class="child last">
	<?php } else { ?>
	<div class="child">
	<?php } ?>
		<?php echo image_tag('/uploads/images/program/'.$item->image, 'alt="'.$item->program.'" title="'.$item->program.'"') ?>
		<p class="date"><?php echo $item->time ?></p>
		<p class="title"><?php echo $item->program ?></p>
		<p class="staff"><?php echo $item->staff ?></p>
	</div>
	<?php endforeach ?>
	</div>
	<?php if ($pager->haveToPaginate())  { ?>
	<div class="pagination" style="font-family: arial; clear:both">
	<?php echo link_to('&#9668;', 'program/index?page='.$pager->getPreviousPage(), 'class=pagination-arrow') ?>
	<?php $links = $pager->getLinks(); foreach ($links as $page): ?>
	<?php echo ($page == $pager->getPage()) ? '<span>'.$page.'</span>' : link_to($page, 'program/index?page='.$page) ;?>
	<?php if ($page != $pager->getCurrentMaxLink()): ?> <?php endif ?>
	<?php endforeach ?>
	<?php echo link_to('&#9658;', 'program/index?page='.$pager->getNextPage(), 'class=pagination-arrow') ?>
	</div>
	<?php } ?>
<?php } else { ?>
	<p>Actualmente no hay programaci&oacute;n disponible.</p>
<?php } ?>
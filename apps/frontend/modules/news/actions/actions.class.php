<?php

/**
 * news actions.
 *
 * @package    fmnuevomundo
 * @subpackage news
 * @author     Diego Baravalle
 */
class newsActions extends fmNuevoMundoActions {

	public function executeIndex(sfWebRequest $request) {
		// Lista de noticias
		$newsQuery = Doctrine_Query::create()
			->from('News n')
			->select('n.id, n.date, n.title, n.short_desc, n.photo_one')
			->orderBy('n.date DESC');
		$this->pager = new sfDoctrinePager('news', 4);
		$this->pager->setQuery($newsQuery);
		$this->pager->setPage($request->getParameter('page', 1));
		$this->pager->init();	
		$this->news = $this->pager->getResults();
	}
	
	public function executeDetail(sfWebRequest $request) {
		$id = $request->getParameter('id', -1);
		$this->item = Doctrine::getTable('News')->find($id);
		if ($this->item == null) {
			$this->forward('news', 'index');
		} else {
			return sfView::SUCCESS;
		}
	}
}

<?php

/**
 * notes actions.
 *
 * @package    fmnuevomundo
 * @subpackage notes
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class notesActions extends fmNuevoMundoActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request) {
		// Lista de noticias
		$newsQuery = Doctrine_Query::create()
			->from('FeaturedNews n')
			->select('n.id, n.date, n.title, n.short_desc, n.photo_one')
			->orderBy('n.date DESC');
		$this->pager = new sfDoctrinePager('FeaturedNews', 4);
		$this->pager->setQuery($newsQuery);
		$this->pager->setPage($request->getParameter('page', 1));
		$this->pager->init();	
		$this->news = $this->pager->getResults();  
	}
  
	public function executeDetail(sfWebRequest $request) {
		$id = $request->getParameter('id', -1);
		$this->item = Doctrine::getTable('FeaturedNews')->find($id);
		if ($this->item == null) {
			$this->forward('notes', 'index');
		} else {
			return sfView::SUCCESS;
		}
	}
}

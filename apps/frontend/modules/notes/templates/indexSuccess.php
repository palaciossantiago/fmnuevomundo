<h2>Notas destacadas</h2>
<?php if (sizeof($news) > 0) { ?>
	<?php foreach($news as $item): ?>
	<div class="noticia">
		<?php if ($item->photo_one) { ?>
		<?php echo link_to(image_tag('/uploads/images/notes/small_'.$item->photo_one,  'alt="'.$item->title.'" title="'.$item->title), 'notes/detail?id='.$item->id) ?>
		<?php } ?>
		<?php use_helper('Date'); ?>
		<p class="date"><?php echo format_date($item->date, 'dd/MM/yyyy'); ?></p>
		<h3><?php echo link_to($item->title, 'notes/detail?id='.$item->id) ?></h3>
		<p><?php echo $item->short_desc ?></p>
		<div class="clear"></div>
	</div>
	<?php endforeach ?>
	<?php if ($pager->haveToPaginate())  { ?>
	<div class="pagination" style="font-family: arial">
	<?php echo link_to('&#9668;', 'notes/index?page='.$pager->getPreviousPage(), 'class=pagination-arrow') ?>
	<?php $links = $pager->getLinks(); foreach ($links as $page): ?>
	<?php echo ($page == $pager->getPage()) ? '<span>'.$page.'</span>' : link_to($page, 'notes/index?page='.$page) ;?>
	<?php if ($page != $pager->getCurrentMaxLink()): ?> <?php endif ?>
	<?php endforeach ?>
	<?php echo link_to('&#9658;', 'notes/index?page='.$pager->getNextPage(), 'class=pagination-arrow') ?>
	</div>
	<?php } ?>
<?php } else { ?>
	<p>Actualmente no hay notas a mostrar.</p>
<?php } ?>
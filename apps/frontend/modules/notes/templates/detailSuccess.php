<div class="detalle-noticia">
	<?php use_helper('Date'); ?>
	<?php if ($item->date != null && $item->date != "") { ?>
		<p class="date"><?php echo format_date($item->date, 'dd/MM/yyyy'); ?></p>
	<?php } ?>
	<h2><?php echo $item->title ?></h2>
	<?php if ($item->photo_one) { ?>
	<div class="detail-photo">
		<?php echo image_tag('/uploads/images/notes/'.$item->photo_one, 'alt='.$item->title.' title='.$item->title.' width=420') ?>
		<?php if ($item->source) { ?>
		<div class="photo-desc">
			<span class="photo-arrow">&#9658;</span><?php echo $item->source ?>
		</div>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<p><?php echo $item->getDescription(ESC_RAW) ?></p>
</div>
	

<h2>Staff</h2>
<?php if (sizeof($staff) > 0) { ?>
	<div id="program">
	<?php foreach($staff as $index => $item): ?>
	<?php if (($index + 1) % 3 == 0) { ?>
	<div class="child last">
	<?php } else { ?>
	<div class="child">
	<?php } ?>
		<?php echo image_tag('/uploads/images/staff/'.$item->photo, 'alt="'.$item->name.'" title="'.$item->name.'"') ?>
		<p class="title"><?php echo $item->name ?></p>
		<p class="staff"><?php echo $item->occupation ?></p>
	</div>
	<?php endforeach ?>
	</div>
	<?php if ($pager->haveToPaginate())  { ?>
	<div class="pagination" style="font-family: arial; clear: both">
	<?php echo link_to('&#9668;', 'staff/index?page='.$pager->getPreviousPage(), 'class=pagination-arrow') ?>
	<?php $links = $pager->getLinks(); foreach ($links as $page): ?>
	<?php echo ($page == $pager->getPage()) ? '<span>'.$page.'</span>' : link_to($page, 'staff/index?page='.$page) ;?>
	<?php if ($page != $pager->getCurrentMaxLink()): ?> <?php endif ?>
	<?php endforeach ?>
	<?php echo link_to('&#9658;', 'staff/index?page='.$pager->getNextPage(), 'class=pagination-arrow') ?>
	</div>
	<?php } ?>
<?php } else { ?>
	<p>Actualmente no hay informaci&oacute;n del staff disponible.</p>
<?php } ?>
<?php

/**
 * staff actions.
 *
 * @package    fmnuevomundo
 * @subpackage staff
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class staffActions   extends fmNuevoMundoActions {

	public function executeIndex(sfWebRequest $request) {
		// Staff
		$staffQuery = Doctrine_Query::create()
			->from('Staff s')
			->select('s.id, s.name, s.occupation, s.photo')
			->orderBy('s.name ASC');
		$this->pager = new sfDoctrinePager('Program', 9);
		$this->pager->setQuery($staffQuery);
		$this->pager->setPage($request->getParameter('page', 1));
		$this->pager->init();	
		$this->staff = $this->pager->getResults();
	}
}